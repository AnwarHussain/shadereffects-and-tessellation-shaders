﻿Shader "Unlit/ElectricityShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	[HDR] _Color ("Color",Color) = (1,1,1,1)
		_ElectricitySpeed ("ElectricitySpeed",vector) = (0.0,0.0,0.0,0.0)
		_ElectricityScale ("ElectricityScale",float) = 0.0
		_ElectricityAmount ("ElectricityAmount",float) = 0.0
		_ElectricityThickness ("ElectricityThickness",float) = 0.0
		_NoiseSpeed("NoiseSpeed",vector) = (0.0,0.0,0.0,0.0)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
		
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
        LOD 300

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag alpha:blend
            // make fog work
           

            #include "UnityCG.cginc"
		// Simple Noise
			inline float noiseRandomValue(float2 uv)
			{
				return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453);
}
			inline float noiseInterpolate(float a,float b, float t)
			{
				return (1.0 - t)*a + (t*b);
}
			inline float valueNoise(float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac(uv);
				f = f * f*f*(3.0 - 2.0*f);

				uv = abs(frac(uv) - 0.5);
				float2 c0 = i + float2(0.0, 0.0);
				float2 c1 = i + float2(1.0, 0.0);
				float2 c2 = i + float2(0.0, 1.0);
				float2 c3 = i + float2(1.0, 1.0);
				float r0 = noiseRandomValue(c0);
				float r1 = noiseRandomValue(c1);
				float r2 = noiseRandomValue(c2);
				float r3 = noiseRandomValue(c3);

				float bottomofGrid = noiseInterpolate(r0, r1, f.x);
				float topofGrid = noiseInterpolate(r2, r3, f.x);
				float t = noiseInterpolate(bottomofGrid, topofGrid, f.y);
				return t;
}
			float SimpleNoise(float2 UV, float Scale)
			{
				float t = 0.0;
				float freq = pow(2.0, float(0));
				float amp = pow(0.5, float(3 - 0));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3 - 1));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3 - 2));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				return t;
}
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _ElectricitySpeed;
				float _ElectricityScale;
				float _ElectricityAmount;
				float _ElectricityThickness;
				float4 _NoiseSpeed;
				float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
                
                return o;
            }

			fixed4 frag(v2f i) : SV_Target
			{
				float resRmap = (pow((SimpleNoise((i.uv * float2(1.0,1.0) + _NoiseSpeed * _Time.y),_ElectricityScale)
				+ SimpleNoise((i.uv * float2(1.0,1.0) + _ElectricitySpeed * _Time.y),_ElectricityScale)),_ElectricityAmount)) * -20.0 + 10.0;

			//Rectangle Shape UV
			float2 d = abs(float2(resRmap,0.0) * 2 - 1) - float2(1.0, 1.5);
			d = 1 - d / fwidth(d);
			float rectangle = saturate(min(d.x, d.y));

			//PolarCoordinates
			float2 delta = i.uv - float2(0.5, 0.5);
			float radius = length(delta) * 2 * 1.04;
			

                // sample the texture
                fixed4 col = (rectangle * clamp(0.0, 1.0, 1.0 - pow(radius, 1.46))) * _Color * i.color;
               
                return col;
            }
            ENDCG
        }
    }
}
