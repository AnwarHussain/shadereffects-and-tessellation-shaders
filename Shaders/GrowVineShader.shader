﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/GrowVineShader"
{
    Properties
	{
	    _Color("Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
			_Grow ("Grow",Range(0,1)) = 0.0
			_Scale ("Scale",float) = -0.1
			_Clip ("Clip",Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }		
			
			
        LOD 300
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:blend vertex:vert 

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.0

        sampler2D _MainTex;
		
        struct Input
        {
            float2 uv_MainTex;
			
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		float _Grow;
		float _Scale;
		float _Clip;
		float GrowVal(float UV, float Growval)
		{
			float d = saturate(UV - Growval);
			return d;
		}
		void vert(inout appdata_full v,out Input IN)
		{
			UNITY_INITIALIZE_OUTPUT(Input, IN);	
			float finalnormal = _Scale * saturate(IN.uv_MainTex.y - _Grow);
		//	v.vertex = mul(unity_WorldToObject, v.vertex);
			v.vertex.xyz += mul(unity_WorldToObject,v.normal) * finalnormal;
			
			
		}

       

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
			
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = 1- saturate(IN.uv_MainTex.y - _Grow);
			clip(o.Alpha - _Clip);
        }
        ENDCG
			Pass
		{
			Tags { "LightMode" = "Always" }
			ZWrite Off
		//Cull Off
		Blend DstColor Zero
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag


			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			float _Grow;
			float _Scale;
			float _Clip;
			

			v2f vert(appdata v)
			{
				v2f o;
				float3 finalnormal = _Scale * saturate(v.uv.y - _Grow);
				v.vertex.xyz += finalnormal * v.normal.xyz;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = v.normal;

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
			col.a = 1 - saturate(i.uv.y - _Grow);
			clip(col.a - _Clip);
			return col;
		}
		ENDCG
		}
    }
    FallBack "Diffuse"
}
