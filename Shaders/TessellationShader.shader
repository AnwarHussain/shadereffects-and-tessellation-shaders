﻿Shader "Tessellation Sample" {
	Properties{
		_EdgeLength("Edge length", Range(2,50)) = 15
		_MainTex("Base (RGB)", 2D) = "white" {}
		_DispTex("Disp Texture", 2D) = "gray" {}
		_NormalMap("Normalmap", 2D) = "bump" {}
		_Displacement("Displacement", Range(0, 1.0)) = 0.3
		_Color("Color", color) = (1,1,1,0)
		_SpecColor("Spec color", color) = (0.5,0.5,0.5,0.5)
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 300

			CGPROGRAM
			#pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:disp tessellate:tessEdge nolightmap
			#pragma target 4.6
			#include "Tessellation.cginc"

			float UnityCalcEdgeTessFactorinShader(float3 wpos0, float3 wpos1, float edgeLen)
{
			
			float dist = distance(0.5 * (wpos0 + wpos1), _WorldSpaceCameraPos);
			
			float len = distance(wpos0, wpos1);
			
			float f = max(len * _ScreenParams.y / (edgeLen * dist), 1.0);
			return f;
		}
		float4 UnityEdgeLengthBasedTessFromUI(float4 v0, float4 v1, float4 v2, float edgeLength)
		{
			float3 pos0 = mul(unity_ObjectToWorld, v0).xyz;
			float3 pos1 = mul(unity_ObjectToWorld, v1).xyz;
			float3 pos2 = mul(unity_ObjectToWorld, v2).xyz;
			float4 tess;
			tess.x = UnityCalcEdgeTessFactorinShader(pos1, pos2, edgeLength);
			tess.y = UnityCalcEdgeTessFactorinShader(pos2, pos0, edgeLength);
			tess.z = UnityCalcEdgeTessFactorinShader(pos0, pos1, edgeLength);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}
			struct appdata {
				float4 vertex : POSITION;
				float4 tangent : TANGENT;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
			};


			float _EdgeLength;

			float4 tessEdge(appdata v0, appdata v1, appdata v2)
			{
				return UnityEdgeLengthBasedTessFromUI(v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
			}

			sampler2D _DispTex;
			float _Displacement;

			void disp(inout appdata v)
			{
				float d = tex2Dlod(_DispTex, float4(v.texcoord.xy,0,0)).r * _Displacement;
				v.vertex.xyz += v.normal * d;
			}

			struct Input {
				float2 uv_MainTex;
			};

			sampler2D _MainTex;
			sampler2D _NormalMap;
			fixed4 _Color;

			void surf(Input IN, inout SurfaceOutput o) {
				half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Specular = 0.2;
				o.Gloss = 1.0;
				o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			}
			ENDCG
		}
			FallBack "Diffuse"
}