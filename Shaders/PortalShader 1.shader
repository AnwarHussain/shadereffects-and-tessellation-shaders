﻿Shader "Custom/PortalShader"
{
    Properties
    {
      [HDR]  _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
       
			_TwirlStrength("Twirl Strength",float) = 6
			_Speed ("Speed",float) = 0.5
			_Scale ("Scale",float) = 2.5
			_DissolveAmount ("DissolveAmount",float) = 2

    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
		
		Cull Off
        LOD 300

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:blend

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
			float2 Twirl(float2 UV,float2 Center, float Strength, float2 Offset)
		{
			float2 delta = UV - Center;
			float angle = Strength * length(delta);
			float x = cos(angle) * delta.x - sin(angle) * delta.y;
			float y = sin(angle) * delta.x + cos(angle) * delta.y;
			return float2(x + Center.x + Offset.x, y + Center.y + Offset.y);
}
		inline float2 voronoi_randomVector(float2 UV,float offset)
		{
			float2x2 m = float2x2(15.27,47.63,99.41,89.98);
			UV = frac(sin(mul(UV, m))*46839.32);
			return float2(sin(UV.y*offset)*0.5 + 0.5, cos(UV.x * offset)*0.5 + 0.5);
}
		void voronoi(float2 UV,float AngleOffset,float CellDensity,out float Out)
		{
			float2 g = floor(UV*CellDensity);
			float2 f = frac(UV * CellDensity);
			float t = 8.0;
			float3 res = float3(8.0, 0.0, 0.0);
			for (int y = -1; y <= 1; y++)
			{
				for (int x = -1; x <= 1; x++)
				{
					float2 lattice = float2(x, y);
					float2 offset = voronoi_randomVector(lattice + g, AngleOffset);
					float d = distance(lattice + offset, f);
					if (d < res.x)
					{
						res = float3(d, offset.x, offset.y);
						Out = res.x;
					}
				}
			}
}
        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        
        fixed4 _Color;
		half _Speed;
		half _TwirlStrength;
		half _Scale;
		half _DissolveAmount;

        

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float Twirlout = Twirl(IN.uv_MainTex, float2(0.5, 0.5), _TwirlStrength, _Speed * _Time.y);
			float Voronoiout;
			voronoi(Twirlout, 2, _Scale,Voronoiout);
			Voronoiout = pow(Voronoiout, _DissolveAmount);
            
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * Voronoiout * _Color;
            o.Albedo = c.rgb;
           
			o.Emission = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
