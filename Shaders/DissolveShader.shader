﻿Shader "Custom/DissolveShader"
{
    Properties
    {
       [HDR] _Color ("EdgeColor", Color) = (1,1,1,1)	
	   _MainTex("MainTexture", 2D) = "white" {}
			_EdgeWidth("EdgeWidth",float) = 0.0
			_NoiseScale("NoiseScale",float) = 0.0
			_Cutoff("AlphaClipCutoff",Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		Cull Off
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:blend

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
			// Simple Noise
			inline float noiseRandomValue(float2 uv)
			{
				return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453);
}
			inline float noiseInterpolate(float a,float b, float t)
			{
				return (1.0 - t)*a + (t*b);
}
			inline float valueNoise(float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac(uv);
				f = f * f*f*(3.0 - 2.0*f);

				uv = abs(frac(uv) - 0.5);
				float2 c0 = i + float2(0.0, 0.0);
				float2 c1 = i + float2(1.0, 0.0);
				float2 c2 = i + float2(0.0, 1.0);
				float2 c3 = i + float2(1.0, 1.0);
				float r0 = noiseRandomValue(c0);
				float r1 = noiseRandomValue(c1);
				float r2 = noiseRandomValue(c2);
				float r3 = noiseRandomValue(c3);

				float bottomofGrid = noiseInterpolate(r0, r1, f.x);
				float topofGrid = noiseInterpolate(r2, r3, f.x);
				float t = noiseInterpolate(bottomofGrid, topofGrid, f.y);
				return t;
}
			float SimpleNoise(float2 UV, float Scale)
			{
				float t = 0.0;
				float freq = pow(2.0, float(0));
				float amp = pow(0.5, float(3 - 0));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3 - 1));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3 - 2));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				return t;
}
       
		sampler2D _MainTex;
        struct Input
        {
			
			float2 uv_MainTex;
        };

       
        fixed4 _Color;
		float _EdgeWidth;
		float _NoiseScale;
		float _Cutoff;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
           // fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = float3(0.7353,0.7353,0.7353);
            
			o.Emission = step((SimpleNoise(IN.uv_MainTex, _NoiseScale)), ((_SinTime.w) + _EdgeWidth)) * _Color;
			o.Alpha = SimpleNoise(IN.uv_MainTex, _NoiseScale* 0.5 + 0.5);
			clip(o.Alpha - (_SinTime.w* 0.5 + 0.5));
        }
        ENDCG
		
    }
    FallBack "Diffuse"
}
