﻿Shader "Unlit/TornadoShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	[HDR] _Color ("Color",Color) = (1,1,1,1)
		_Dissolve ("Dissolve",Range(0,1)) = 0.0
		_NoiseSpeed ("NoiseSpeed",vector) = (0.0,0.0,0.0,0.0)
		_NoiseScale ("NoiseScale",float) = 0.0
		_NoisePower ("NoisePower",float) = 0.0
		_TwirlCenter ("TwirlCenter",vector) = (0.0,0.0,0.0,0.0)
		_TwirlSpeed ("TwirlSpeed",vector) = (0.0,0.0,0.0,0.0)
		_TwirlAmount ("TwirlAmount",float) = 0.0
		_WobbleAmount ("WobbleAmount",Range(0,1)) = 0.0
		_WobbleSpeed ("WobbleSpeed",float) = 0.0
		_WobbleFrequency ("WobbleFrequency",float) = 0.0
		_WobbleDistance ("WobbleDistance",float) = 0.0

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
		//Twirl
		float2 Twirl(float2 UV, float2 Center, float Strength, float2 Offset)
{
	float2 delta = UV - Center;
	float angle = Strength * length(delta);
	float x = cos(angle) * delta.x - sin(angle) * delta.y;
	float y = sin(angle) * delta.x + cos(angle) * delta.y;
	return float2(x + Center.x + Offset.x, y + Center.y + Offset.y);
}


		//RadialShear
		float2 RadialShear(float2 UV, float2 Center, float Strength, float2 Offset)
{
	float2 delta = UV - Center;
	float delta2 = dot(delta.xy, delta.xy);
	float2 delta_offset = delta2 * Strength;
	return (UV + float2(delta.y, -delta.x) * delta_offset + Offset);
}


		//SimpleNoise
		inline float noiserandomValue(float2 uv)
{
	return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453);
}

inline float noiseinterpolate(float a, float b, float t)
{
	return (1.0 - t)*a + (t*b);
}

inline float valueNoise(float2 uv)
{
	float2 i = floor(uv);
	float2 f = frac(uv);
	f = f * f * (3.0 - 2.0 * f);

	uv = abs(frac(uv) - 0.5);
	float2 c0 = i + float2(0.0, 0.0);
	float2 c1 = i + float2(1.0, 0.0);
	float2 c2 = i + float2(0.0, 1.0);
	float2 c3 = i + float2(1.0, 1.0);
	float r0 = noiserandomValue(c0);
	float r1 = noiserandomValue(c1);
	float r2 = noiserandomValue(c2);
	float r3 = noiserandomValue(c3);

	float bottomOfGrid = noiseinterpolate(r0, r1, f.x);
	float topOfGrid = noiseinterpolate(r2, r3, f.x);
	float t = noiseinterpolate(bottomOfGrid, topOfGrid, f.y);
	return t;
}

float SimpleNoise(float2 UV, float Scale)
{
	float t = 0.0;

	float freq = pow(2.0, float(0));
	float amp = pow(0.5, float(3 - 0));
	t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

	freq = pow(2.0, float(1));
	amp = pow(0.5, float(3 - 1));
	t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

	freq = pow(2.0, float(2));
	amp = pow(0.5, float(3 - 2));
	t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

	return t;
}
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 WorldSpacePosition : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float3 WorldSpacePosition : TEXCOORD1;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;
			fixed _Dissolve;
			float4 _NoiseSpeed;
			fixed _NoiseScale;
			fixed _NoisePower;
			float4 _TwirlCenter;
			float4 _TwirlSpeed;
			float _TwirlAmount;
			float _WobbleAmount;
			float _WobbleSpeed;
			float _WobbleFrequency;
			float _WobbleDistance;

            v2f vert (appdata v)
            {
                v2f o;
				float3 WorldSpacePosition = mul(UNITY_MATRIX_M, v.vertex).xyz;
				o.WorldSpacePosition = WorldSpacePosition;
				float d = (_Time.y * _WobbleSpeed) + (abs(o.WorldSpacePosition).x * _WobbleFrequency + abs(o.WorldSpacePosition).y * _WobbleFrequency);
				
				float4 vertex = float4((v.vertex.x + sin(d) * _WobbleDistance),v.vertex.y,v.vertex.z,v.vertex.w);
				float4 w = lerp(v.vertex, vertex, _WobbleAmount);
                o.vertex = UnityObjectToClipPos(w);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float2 TwirlVal = Twirl(i.uv,_TwirlCenter.xy,_TwirlAmount,(_Time.y * _TwirlSpeed.xy));
				float2 RadialVal = RadialShear(i.uv, float2(0.5, 0.5), float2(10.0, 10.0), _Time.y * _NoiseSpeed.xy);
				float s = pow(SimpleNoise(RadialVal, _NoiseScale) * SimpleNoise(TwirlVal, 500), _NoisePower);
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv)*_Color * s;
				col.a = _Dissolve;
				clip(col.a - s);
                return col;
            }
            ENDCG
        }
    }
}
