﻿Shader "Custom/FlamesShader"
{
    Properties
	{
	   [HDR] _Color("Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DistortionAmount ("DistortionAmount",float) = 0.1
			_DistortionSpeed ("DistortionSpeed",vector) = (0.0,-0.3,0.0,0.0)
			_DistortionScale ("DistortionScale",float) = 5
			_DissolveSpeed ("DissolveSpeed",vector) = (-0.1,-0.5,0.0,0.0)
			_DissolveScale ("DissolveScale",float) = 2
			_DissolveAmount ("DissolveAmount",float) = 1.2
        
    }
    SubShader
    {
	   Tags { "RenderType" = "Transparent" }

		Cull Off
		LOD 300

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:blend

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
			
		
		//Voronoi Noise
			inline float2 voronoi_randomVector(float2 UV,float offset)
		{
			float2x2 m = float2x2(15.27,47.63,99.41,89.98);
			UV = frac(sin(mul(UV, m))*46839.32);
			return float2(sin(UV.y*offset)*0.5 + 0.5, cos(UV.x * offset)*0.5 + 0.5);
}
		void voronoi(float2 UV,float AngleOffset,float CellDensity,out float Out)
		{
			float2 g = floor(UV*CellDensity);
			float2 f = frac(UV * CellDensity);
			float t = 8.0;
			float3 res = float3(8.0, 0.0, 0.0);
			for (int y = -1; y <= 1; y++)
			{
				for (int x = -1; x <= 1; x++)
				{
					float2 lattice = float2(x, y);
					float2 offset = voronoi_randomVector(lattice + g, AngleOffset);
					float d = distance(lattice + offset, f);
					if (d < res.x)
					{
						res = float3(d, offset.x, offset.y);
						Out = res.x;
					}
				}
			}
}
		//Gradient Noise
		float2 gradientNoise_dir(float2 p)
		{
			p = p % 289;
			float x = (34 * p.x + 1) * p.x % 289 + p.y;
			x = (34 * x + 1) * x % 289;
			x = frac(x / 41) * 2 - 1;
			return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}
		float gradientNoise(float2 p)
		{
			float2 ip = floor(p);
			float2 fp = frac(p);
			float d00 = dot(gradientNoise_dir(ip), fp);
			float d01 = dot(gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
			float d10 = dot(gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
			float d11 = dot(gradientNoise_dir(ip + float2(1, 1)),fp - float2(1, 1));
			fp = fp * fp*fp*(fp*(fp * 6 - 15) + 10);
			return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
}
		float GradientNoiseResult(float2 UV,float Scale)
		{
			return gradientNoise(UV*Scale) + 0.5;
}
        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        
        fixed4 _Color;
		float _DistortionAmount;
		float4 _DistortionSpeed;
		float _DistortionScale;
		float4 _DissolveSpeed;
		float _DissolveScale;
		float _DissolveAmount;
       

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			
			float gradientresult = GradientNoiseResult(IN.uv_MainTex * float2(1.0, 1.0) + _DistortionSpeed.xy * _Time.y, _DistortionScale);
			float voronoiresult;
			voronoi(IN.uv_MainTex * float2(1.0, 1.0) + _DistortionSpeed.xy * _Time.y, 2.0, _DissolveScale, voronoiresult);
			voronoiresult = pow(voronoiresult, _DissolveAmount);
            
			fixed4 c = tex2D(_MainTex, lerp(IN.uv_MainTex, gradientresult, _DistortionAmount)) * mul(voronoiresult , gradientresult) * _Color;
            o.Albedo = c.rgb;
            
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
