Shader "Unlit/KiChargeEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	[HDR] _Color ("Color",Color) = (1,1,1,1)
		_MainTexSpeed ("MainTexSpeed",vector) = (0.0,0.0,0.0,0.0)
		_MainTexTiling("MainTexTiling",vector) = (0.0,0.0,0.0,0.0)
		_DissolveScale("DissolveScale",float) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
		Cull Off
		Blend One One
        LOD 300

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 


            #include "UnityCG.cginc"
		inline float2 unity_voronoi_noise_randomVector(float2 UV, float offset)
{
	float2x2 m = float2x2(15.27, 47.63, 99.41, 89.98);
	UV = frac(sin(mul(UV, m)) * 46839.32);
	return float2(sin(UV.y*+offset)*0.5 + 0.5, cos(UV.x*offset)*0.5 + 0.5);
}

void Voronoi(float2 UV, float AngleOffset, float CellDensity,out float Out)
{
	float2 g = floor(UV * CellDensity);
	float2 f = frac(UV * CellDensity);
	float t = 8.0;
	float3 res = float3(8.0, 0.0, 0.0);

	for (int y = -1; y <= 1; y++)
	{
		for (int x = -1; x <= 1; x++)
		{
			float2 lattice = float2(x,y);
			float2 offset = unity_voronoi_noise_randomVector(lattice + g, AngleOffset);
			float d = distance(lattice + offset, f);
			if (d < res.x)
			{
				res = float3(d, offset.x, offset.y);
				Out = res.x;
				
			}
		}
	}
}
            struct appdata
            {
                float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
				float4 color : COLOR;
            };

            struct v2f
            {
				float4 uv : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
				float4 color : COLOR;
                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;
			float4 _MainTexSpeed;
			float4 _MainTexTiling;
			float _DissolveScale;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;//TRANSFORM_TEX(v.uv, _MainTex);
				o.uv1 = v.uv1;//TRANSFORM_TEX(v.uv2, _MainTex);
				o.color = v.color;
               
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv*_MainTexTiling +float2(0.0,i.uv1.z));
			float voronoiresult;
			Voronoi(i.uv, 10.0, _DissolveScale, voronoiresult);
			col *= pow(voronoiresult, i.uv1.w);
			col *= _Color * i.color;
                
                return col;
            }
            ENDCG
        }
    }
}
