﻿Shader "Unlit/LaserBeamSHader"
{
    Properties
	{
		_MainTex("MainTexture", 2D) = "white" {}
		[HDR] _Color("Color",Color) = (1,1,1,1)
			_MainSpeed("MainSpeed",vector) = (-0.5,0,0,0)
			_Mask("Mask",2D) = "white" {}
			_NoiseSpeed("NoiseSpeed",vector) = (-0.7,0,0,0)
				_NoiseScale("NoiseScale",float) = 25.0
				_NoisePower("NoisePower",float) = 2.0
				_NoiseAmount("NoiseAmount",Range(0,1)) = 0.05
				_DissolveAmount("DissolveAmount",Range(0,1)) = 0.6
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"  }
        LOD 300
		ZWrite Off
		
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert 
            #pragma fragment frag alpha:blend
			


            #include "UnityCG.cginc"
			// Simple Noise
			inline float noiseRandomValue(float2 uv)
			{
				return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453);
}
			inline float noiseInterpolate(float a,float b, float t)
			{
				return (1.0 - t)*a + (t*b);
}
			inline float valueNoise(float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac(uv);
				f = f * f*f*(3.0 - 2.0*f);

				uv = abs(frac(uv) - 0.5);
				float2 c0 = i + float2(0.0, 0.0);
				float2 c1 = i + float2(1.0, 0.0);
				float2 c2 = i + float2(0.0, 1.0);
				float2 c3 = i + float2(1.0, 1.0);
				float r0 = noiseRandomValue(c0);
				float r1 = noiseRandomValue(c1);
				float r2 = noiseRandomValue(c2);
				float r3 = noiseRandomValue(c3);

				float bottomofGrid = noiseInterpolate(r0, r1, f.x);
				float topofGrid = noiseInterpolate(r2, r3, f.x);
				float t = noiseInterpolate(bottomofGrid, topofGrid, f.y);
				return t;
}
			float SimpleNoise(float2 UV, float Scale)
			{
				float t = 0.0;
				float freq = pow(2.0, float(0));
				float amp = pow(0.5, float(3 - 0));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3 - 1));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3 - 2));
				t += valueNoise(float2(UV.x*Scale / freq, UV.y*Scale / freq))*amp;

				return t;
}

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
			sampler2D _Mask;
			float4 _Color;
			float4 _MainSpeed;
			float4 _NoiseSpeed;
			float _NoiseScale;
			float _NoiseAmount;
			float _NoisePower;
			float _DissolveAmount;
			float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float NoiseResult = pow(SimpleNoise(i.uv + (_NoiseSpeed * _Time.y),_NoiseScale),_NoisePower);				

                // sample the texture
                fixed4 MainTexres = tex2D(_MainTex, (_MainSpeed * _Time.y).xy + lerp(i.uv, float4(NoiseResult, 0, 0, 0), _NoiseAmount).xy);
				fixed4 MaskTex = tex2D(_Mask, i.uv);

				fixed4 MainTexresDissolve = lerp(MainTexres, fixed4((MainTexres.a * NoiseResult),0,0,0), _DissolveAmount);

				fixed4 col = fixed4(((MaskTex * MainTexresDissolve) * _Color * i.color).rgb, (MainTexresDissolve.a*MaskTex.r * i.color.a));
                
                return col;
            }
            ENDCG
        }
    }
}
