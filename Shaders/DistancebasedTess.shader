﻿Shader "Distance Based Tessllation Sample" {
	Properties{
		_Tess ("Tessellation", Range(1,32)) = 4
		_MainTex ("Main", 2D) = "white" {}
		_DispTex ("Disptex", 2D) = "gray" {}
		_NormalMap ("NormalMap", 2D) = "bump" {}
		_Displacement ("Displacement", Range(0,1.0)) = 0.3
		_Color ("Color",color) = (1,1,1,0)
		_SpecColor ("Spec color", color) = (0.5,0.5,0.5,0.5)
	}
		SubShader{
			Tags { "RenderType"="Opaque"}
			LOD 300
			CGPROGRAM
#pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:disp tessellate:tessDistance nolightmap
#pragma target 4.6
#include "Tessellation.cginc"
			float UnityCalcDistanceTessFactorFromCamera(float4 vertex, float minDist, float maxDist, float tess)
{
	float3 wpos = mul(unity_ObjectToWorld,vertex).xyz;
	float dist = distance(wpos, _WorldSpaceCameraPos);
	float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
	return f;
}
		float4 UnityCalcTriEdgeTessFactorsFromCamera(float3 triVertexFactors)
{
	float4 tess;
	tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
	tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
	tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
	tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
	return tess;
}
			float4 UnityDistanceBasedTessFromCamera(float4 v0, float4 v1, float4 v2, float minDist, float maxDist, float tess)
{
	float3 f;
	f.x = UnityCalcDistanceTessFactorFromCamera(v0,minDist,maxDist,tess);
	f.y = UnityCalcDistanceTessFactorFromCamera(v1,minDist,maxDist,tess);
	f.z = UnityCalcDistanceTessFactorFromCamera(v2,minDist,maxDist,tess);

	return UnityCalcTriEdgeTessFactorsFromCamera(f);
}
			struct appdata
		{
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float2 texcoord : TEXCOORD0;
};
		float _Tess;
		float4 tessDistance(appdata v0, appdata v1, appdata v2) {
			float minDist = 10.0;
			float maxDist = 25.0;
			return UnityDistanceBasedTessFromCamera(v0.vertex, v1.vertex, v2.vertex, minDist, maxDist, _Tess);
		}

		sampler2D _DispTex;
		float _Displacement;

		void disp(inout appdata v)
		{
			float d = tex2Dlod(_DispTex, float4(v.texcoord.xy, 0, 0)).r * _Displacement;
			v.vertex.xyz += v.normal*d;
		}

		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _MainTex;
		sampler2D _NormalMap;
		fixed4 _Color;

		void surf(Input IN, inout SurfaceOutput o) {
			half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Specular = 0.2;
			o.Gloss = 1.0;
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
		}
		ENDCG
		}
			FallBack "Diffuse"

}